---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
# Embedded files
404dbfda7875192f0172fbd14478387614f4292e: $$a_1$$
27a33cd9ae4b835a7daafa8e7e8747352324fa9d: $$a_2$$
38dd01addc7804cc6290838b9d38c909870743a4: $$a_3$$
2b831ac132b7d994a08b56850c8a04e390cbdb46: $$a_4$$
59a3b2de6e62f66996d37fff29cb2c7d2b112fe7: $$a_5$$
6619039ff3749104ea7e84c718e360466e167b0b: $$a_1^{T}x = b_1$$
d27f548af5534691c8c3af1cd8ec3b22868ff31e: $$a_2^{T}x = b_2$$
1f450e3f97fc50f6daf8a194c713a796eb0639b1: $$a_3^{T}x = b_3$$
9f4cdbf344192e61c09930433331a217db23032b: $$a_4^{T}x = b_4$$
4c0a589a5727939a6a961077cf665356eaa225b4: $$a_5^{T}x = b_5$$

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.6",
	"elements": [
		{
			"type": "line",
			"version": 310,
			"versionNonce": 1645239763,
			"isDeleted": false,
			"id": "jPvGWM1zmOoc2cLytyqlS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 144.563720703125,
			"y": 877.0030212402344,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"width": 395.138916015625,
			"height": 383.33331298828125,
			"seed": 579488893,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072424918,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					170.8333740234375,
					-215.27777099609375
				],
				[
					389.5833740234375,
					-122.22222900390625
				],
				[
					395.138916015625,
					118.0555419921875
				],
				[
					152.77783203125,
					168.0555419921875
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 162,
			"versionNonce": 1275813085,
			"isDeleted": false,
			"id": "bMTdAP4G6C23hQ3Hw9wPe",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 73.03594970703125,
			"y": 794.3641357421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 294.44439697265625,
			"height": 326.3888854980469,
			"seed": 1967303421,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424918,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					294.44439697265625,
					326.3888854980469
				]
			]
		},
		{
			"type": "line",
			"version": 83,
			"versionNonce": 1384852339,
			"isDeleted": false,
			"id": "_aZhkGGBfbmxn2tjoU-Ry",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 77.20263671875,
			"y": 958.9474792480469,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 305.5555419921875,
			"height": 381.25001525878906,
			"seed": 1390612179,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424918,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					305.5555419921875,
					-381.25001525878906
				]
			]
		},
		{
			"type": "line",
			"version": 50,
			"versionNonce": 1336260925,
			"isDeleted": false,
			"id": "LbE6HNsEtfORskGjX6B6i",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 202.8970947265625,
			"y": 613.8085784912109,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 415.2777099609375,
			"height": 176.3888702392578,
			"seed": 276822707,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424918,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					415.2777099609375,
					176.3888702392578
				]
			]
		},
		{
			"type": "line",
			"version": 84,
			"versionNonce": 516623517,
			"isDeleted": false,
			"id": "GqLX1bznbnR2jhUyEsofn",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 531.369140625,
			"y": 607.5585632324219,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 9.02783203125,
			"height": 505.5555419921875,
			"seed": 1324358461,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072515324,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					9.02783203125,
					505.5555419921875
				]
			]
		},
		{
			"type": "line",
			"version": 131,
			"versionNonce": 1014335901,
			"isDeleted": false,
			"id": "3yaT2qtWwuM-5Ll6aN99O",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 671.6470947265625,
			"y": 966.5863342285156,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 476.388916015625,
			"height": 99.3055419921875,
			"seed": 608087389,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-476.388916015625,
					99.3055419921875
				]
			]
		},
		{
			"type": "arrow",
			"version": 274,
			"versionNonce": 381102771,
			"isDeleted": false,
			"id": "XEESygewj6TV5-hy4EYl0",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 237.6192626953125,
			"y": 763.1141357421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 36.8055419921875,
			"height": 28.472198486328125,
			"seed": 1456508349,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "C5kHgzCRGsU3cmA5FAwsU",
				"focus": 0.31199735508131116,
				"gap": 7.638946533203125
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					36.8055419921875,
					28.472198486328125
				]
			]
		},
		{
			"type": "arrow",
			"version": 64,
			"versionNonce": 1809893811,
			"isDeleted": false,
			"id": "pwtq4yt_xf39dVXDao8fr",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 414.0081787109375,
			"y": 1019.3641052246094,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 16.087375261982857,
			"height": 61.879304645830985,
			"seed": 458296669,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072541287,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "UuGqBMaLrbPt_NGLOaUbM",
				"focus": -0.07488261931439533,
				"gap": 15.908206193541446
			},
			"endBinding": {
				"elementId": "FzxE4EWiE5VtNVD8KA-r_",
				"focus": 1.693927024084529,
				"gap": 12.222338867187545
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-16.087375261982857,
					-61.879304645830985
				]
			]
		},
		{
			"type": "arrow",
			"version": 39,
			"versionNonce": 1720015955,
			"isDeleted": false,
			"id": "YNOl-kDm2fnAz0jvehWTm",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 407.7581787109375,
			"y": 701.3085632324219,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14.5833740234375,
			"height": 36.111114501953125,
			"seed": 1331040829,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "_LsOL-pWeWSrWt1na6j-Q",
				"focus": 1.0295129357388464,
				"gap": 9.916552734374932
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-14.5833740234375,
					36.111114501953125
				]
			]
		},
		{
			"type": "arrow",
			"version": 140,
			"versionNonce": 877364829,
			"isDeleted": false,
			"id": "YSVGC_V03iLXI7MdhYPA_",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 532.063720703125,
			"y": 865.1974487304688,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 67.3612060546875,
			"height": 0.453355958884913,
			"seed": 868333565,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "Qsb0Xx7HUbXWZ7XoPOpzA",
				"focus": 0.2853100813183204,
				"gap": 14.361132812499932
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-67.3612060546875,
					-0.453355958884913
				]
			]
		},
		{
			"type": "arrow",
			"version": 44,
			"versionNonce": 752638451,
			"isDeleted": false,
			"id": "fGkp4ZQiUg5j61IxY3GBb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 214.0081787109375,
			"y": 952.6974792480469,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 29.861083984375,
			"height": 26.388916015625,
			"seed": 2127639901,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "VGbZA7lS",
				"focus": 1.3936918480387357,
				"gap": 12.71405029296875
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					29.861083984375,
					-26.388916015625
				]
			]
		},
		{
			"type": "image",
			"version": 64,
			"versionNonce": 1629795005,
			"isDeleted": false,
			"id": "VGbZA7lS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 256.58331298828125,
			"y": 924.4444274902344,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.55556640625,
			"height": 19.72222900390625,
			"seed": 6280,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "fGkp4ZQiUg5j61IxY3GBb",
					"type": "arrow"
				}
			],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "404dbfda7875192f0172fbd14478387614f4292e",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 121,
			"versionNonce": 918725523,
			"isDeleted": false,
			"id": "C5kHgzCRGsU3cmA5FAwsU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 272.36931152343755,
			"y": 799.2252807617188,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.55556640625,
			"height": 19.72222900390625,
			"seed": 73107,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "XEESygewj6TV5-hy4EYl0",
					"type": "arrow"
				}
			],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "27a33cd9ae4b835a7daafa8e7e8747352324fa9d",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 100,
			"versionNonce": 114905885,
			"isDeleted": false,
			"id": "_LsOL-pWeWSrWt1na6j-Q",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 351.70268554687505,
			"y": 740.80859375,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.55556640625,
			"height": 19.72222900390625,
			"seed": 199837597,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "YNOl-kDm2fnAz0jvehWTm",
					"type": "arrow"
				}
			],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "38dd01addc7804cc6290838b9d38c909870743a4",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 187,
			"versionNonce": 1819195699,
			"isDeleted": false,
			"id": "Qsb0Xx7HUbXWZ7XoPOpzA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 418.78581542968755,
			"y": 851.8363647460938,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.55556640625,
			"height": 19.72222900390625,
			"seed": 1058281491,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "YSVGC_V03iLXI7MdhYPA_",
					"type": "arrow"
				}
			],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "2b831ac132b7d994a08b56850c8a04e390cbdb46",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 127,
			"versionNonce": 1641736061,
			"isDeleted": false,
			"id": "FzxE4EWiE5VtNVD8KA-r_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 408.86943359375005,
			"y": 930.919677734375,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.55556640625,
			"height": 19.72222900390625,
			"seed": 380921821,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "pwtq4yt_xf39dVXDao8fr",
					"type": "arrow"
				}
			],
			"updated": 1714072424919,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "59a3b2de6e62f66996d37fff29cb2c7d2b112fe7",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 342,
			"versionNonce": 571623731,
			"isDeleted": false,
			"id": "wDnBtF2k",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.6011964598123267,
			"x": 156.92596435546875,
			"y": 964.1943969726562,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 80.01390075683614,
			"height": 22.861114501953182,
			"seed": 55690,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072669185,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "6619039ff3749104ea7e84c718e360466e167b0b",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 492,
			"versionNonce": 1629757523,
			"isDeleted": false,
			"id": "2RmZ0dvc31v7GKdp-Tvbb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 5.4357692573144565,
			"x": 168.97351837158192,
			"y": 737.2946929931641,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 80.01390075683614,
			"height": 22.861114501953182,
			"seed": 1825960125,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072432488,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "d27f548af5534691c8c3af1cd8ec3b22868ff31e",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 676,
			"versionNonce": 735179507,
			"isDeleted": false,
			"id": "V2s4LmGqJbCV3KiliGUAQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.4049951050592533,
			"x": 395.1516466614057,
			"y": 657.5092553024137,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 77.87981214593853,
			"height": 23.487562393219555,
			"seed": 415327379,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072457411,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "1f450e3f97fc50f6daf8a194c713a796eb0639b1",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 1603,
			"versionNonce": 454465971,
			"isDeleted": false,
			"id": "YYe01UOyppSeqwh-j2xrh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 1.4893087729487124,
			"x": 524.2233963012694,
			"y": 849.1464996337892,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 80.01390075683614,
			"height": 22.86111450195318,
			"seed": 462557747,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714072655773,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "9f4cdbf344192e61c09930433331a217db23032b",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "image",
			"version": 1629,
			"versionNonce": 1126374227,
			"isDeleted": false,
			"id": "UuGqBMaLrbPt_NGLOaUbM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 6.208695761196212,
			"x": 385.20731072390583,
			"y": 1034.592553031906,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 77.87981214593852,
			"height": 23.487562393219555,
			"seed": 313183837,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "pwtq4yt_xf39dVXDao8fr",
					"type": "arrow"
				}
			],
			"updated": 1714072541287,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "4c0a589a5727939a6a961077cf665356eaa225b4",
			"scale": [
				1,
				1
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "#b2f2bb",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 198.07683592274475,
		"scrollY": -554.5942882905404,
		"zoom": {
			"value": 1.5
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		},
		"objectsSnapModeEnabled": false
	},
	"files": {}
}
```
%%
---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
X1 ^zgSFje8p

X2 ^BSzmGpuw

X3 ^ofoZYixG

x3 = 0 ^S7wXmEBC

x2 = 0 ^KizLyOnR

we project the
feasible set in the
2d plane ^xA8SxC3K

x1 = 0 ^3QwPk59w

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.23",
	"elements": [
		{
			"id": "m_C1FEs2qQnJnupcm5LT0",
			"type": "arrow",
			"x": -185.43283081054688,
			"y": 74.58086395263672,
			"width": 0,
			"height": 391.77378845214844,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1512220594,
			"version": 88,
			"versionNonce": 1793470446,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816285207,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-391.77378845214844
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "ofoZYixG",
				"focus": 2.2442928639175386,
				"gap": 15.914497375488281
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "ndUB7MaUGUlHGsss84cwO",
			"type": "arrow",
			"x": -249.33383178710938,
			"y": 53.28050994873047,
			"width": 461.760498046875,
			"height": 0,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1610136686,
			"version": 96,
			"versionNonce": 1103597042,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816285207,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					461.760498046875,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "Ct67J2KfWh2kZPMB0Qfuv",
			"type": "arrow",
			"x": -189.09833595969468,
			"y": 55.42439200661397,
			"width": 355.535400390625,
			"height": 177.24910389293325,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 499419694,
			"version": 344,
			"versionNonce": 1118550126,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816682888,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					355.535400390625,
					-177.24910389293325
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "zgSFje8p",
			"type": "text",
			"x": 212.42678833007812,
			"y": 74.58086395263672,
			"width": 17.379989624023438,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 115151666,
			"version": 50,
			"versionNonce": 1699020722,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816285208,
			"link": null,
			"locked": false,
			"text": "X1",
			"rawText": "X1",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "X1",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 105,
			"versionNonce": 358977774,
			"isDeleted": false,
			"id": "BSzmGpuw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 120.85220544988465,
			"y": -93.33149788596404,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 26.199981689453125,
			"height": 25,
			"seed": 1201212722,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713816677069,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "X2",
			"rawText": "X2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "X2",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 598620530,
			"isDeleted": false,
			"id": "ofoZYixG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -169.5183334350586,
			"y": -333.13660430908203,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 25.579986572265625,
			"height": 25,
			"seed": 489709230,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "m_C1FEs2qQnJnupcm5LT0",
					"type": "arrow"
				}
			],
			"updated": 1713816285208,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "X3",
			"rawText": "X3",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "X3",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"id": "U2pXR6R-kJ-6JbXTmCQNa",
			"type": "line",
			"x": 17.680816650390625,
			"y": 54.80199432373047,
			"width": 207.6781005859375,
			"height": 236.585693359375,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1744884526,
			"version": 201,
			"versionNonce": 262494834,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713817090168,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-202.35302734375,
					-236.585693359375
				],
				[
					5.3250732421875,
					-104.21942138671875
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				3.04296875,
				1.521484375
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "7Jg-5QkYCzVprgYV5FqDZ",
			"type": "line",
			"x": 379.9103232492105,
			"y": 43.49609828644003,
			"width": 264.0688272664071,
			"height": 218.56540001710485,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1179905010,
			"version": 551,
			"versionNonce": 1462940782,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713817094037,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					264.0688272664071,
					0.7459730315995898
				],
				[
					126.81266226221766,
					-217.81942698550526
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0,
				3.415048412535498
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "cpGDIb-1rKpvw7mgXscRg",
			"type": "arrow",
			"x": -182.0535307971315,
			"y": -178.99557882189134,
			"width": 688.2814601412506,
			"height": 135.3815799100248,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1044251054,
			"version": 462,
			"versionNonce": 836476082,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713817146859,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					355.3934833910769,
					-128.36954875645756
				],
				[
					688.2814601412506,
					7.01203115356725
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot"
		},
		{
			"id": "1Kx3BTG6BCJixKqTdGHhO",
			"type": "line",
			"x": 552.0304510918769,
			"y": -252.18725999020722,
			"width": 217.55655725700734,
			"height": 376.81901068890403,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 530001390,
			"version": 166,
			"versionNonce": 702729262,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816751645,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-217.55655725700734,
					376.81901068890403
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "2yj-gVxGJXLjpI2sIP1gb",
			"type": "line",
			"x": 309.0937268745813,
			"y": 43.28204023109487,
			"width": 409.4087357954545,
			"height": 0,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1274519854,
			"version": 122,
			"versionNonce": 737173422,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816748640,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					409.4087357954545,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "BNFOYEiZEuIA7meCYygo5",
			"type": "line",
			"x": 700.5216166117972,
			"y": 135.95227283052668,
			"width": 244.12375710227258,
			"height": 393.5026411576704,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 159781486,
			"version": 328,
			"versionNonce": 8707506,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816970539,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-244.12375710227258,
					-393.5026411576704
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "S7wXmEBC",
			"type": "text",
			"x": 725.4183007382178,
			"y": 31.525448434219868,
			"width": 70.95997619628906,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1673621682,
			"version": 57,
			"versionNonce": 1955998962,
			"isDeleted": false,
			"boundElements": [],
			"updated": 1713816744437,
			"link": null,
			"locked": false,
			"text": "x3 = 0",
			"rawText": "x3 = 0",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "x3 = 0",
			"lineHeight": 1.25
		},
		{
			"id": "KizLyOnR",
			"type": "text",
			"x": 561.5163786856608,
			"y": -276.91428489763376,
			"width": 71.57997131347656,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1073161266,
			"version": 79,
			"versionNonce": 626181362,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816937347,
			"link": null,
			"locked": false,
			"text": "x2 = 0",
			"rawText": "x2 = 0",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "x2 = 0",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 872,
			"versionNonce": 1214235122,
			"isDeleted": false,
			"id": "rr1PLwE44el_zmAlQedlw",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 23.73860209631647,
			"y": -51.990562520257356,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 615.6668250205123,
			"height": 235.65906792493962,
			"seed": 1480190574,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713817154332,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					192.87483992516798,
					-141.50936365347462
				],
				[
					615.6668250205123,
					94.149704271465
				]
			]
		},
		{
			"type": "arrow",
			"version": 888,
			"versionNonce": 607164334,
			"isDeleted": false,
			"id": "ZVzlUGN2_VWrji3lOttk6",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 18.1723623058339,
			"y": 47.06566771991652,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 361.1693108159667,
			"height": 71.66092504374456,
			"seed": 744796014,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713816711988,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					152.7637559407928,
					-71.66092504374456
				],
				[
					361.1693108159667,
					-6.127783743449811
				]
			]
		},
		{
			"id": "xA8SxC3K",
			"type": "text",
			"x": 117.93411439233682,
			"y": -401.3970258200414,
			"width": 184.53982543945312,
			"height": 75,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 960286254,
			"version": 73,
			"versionNonce": 169528626,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713816853477,
			"link": null,
			"locked": false,
			"text": "we project the\nfeasible set in the\n2d plane",
			"rawText": "we project the\nfeasible set in the\n2d plane",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "top",
			"baseline": 67,
			"containerId": null,
			"originalText": "we project the\nfeasible set in the\n2d plane",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 147,
			"versionNonce": 1038565102,
			"isDeleted": false,
			"id": "3QwPk59w",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 409.10011683929156,
			"y": -287.3306140213912,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 62.759979248046875,
			"height": 25,
			"seed": 891593458,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713816998713,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "x1 = 0",
			"rawText": "x1 = 0",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "x1 = 0",
			"lineHeight": 1.25,
			"baseline": 17
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "#b2f2bb",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": "dot",
		"currentItemEndArrowhead": "dot",
		"scrollX": 298.1456763450527,
		"scrollY": 830.1533631716546,
		"zoom": {
			"value": 0.7000000000000001
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%
---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
(A4,C4) ^ITGOIHXR

b ^OuJR8qzb

A1 ^2YL3vkEB

A2 ^YTqQwyLw

(A1,C1) ^ucKQtBGi

(A3,C3) ^ajIPCVDS

z ^3pRldLkS

(A2,C2) ^iGCoONQL

A4 ^nXi9JHhG

4 enters
the basis ^BgdawluR

The simplex generated
by 1,2,4 intersect the
requirement line first:
3 is left out and leaves
the basis ^fXHShpFv

We start from the
basic simplex 
associated to
1,2,3 ^RO28bLOZ

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.6",
	"elements": [
		{
			"type": "line",
			"version": 478,
			"versionNonce": 428728852,
			"isDeleted": false,
			"id": "4DGaPtOkINiQjm62Fa8RP",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -140.82615371497127,
			"y": 95.52751747432904,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 270.81916624894325,
			"height": 151.84002398222196,
			"seed": 1221444908,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					214.16240153136937,
					95.18335017919654
				],
				[
					270.81916624894325,
					-56.65667380302543
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 80,
			"versionNonce": 2017982764,
			"isDeleted": false,
			"id": "9YJ59TMyimHwO0Fd5ToNb",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 19.406993609357414,
			"y": 103.44597749815898,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 160.21977834883552,
			"seed": 281559980,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-160.21977834883552
				]
			]
		},
		{
			"type": "ellipse",
			"version": 600,
			"versionNonce": 187727764,
			"isDeleted": false,
			"id": "Dh47_jIu1qGNn5OK2FbFC",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 11.350826154222077,
			"y": -64.77135319528617,
			"strokeColor": "#f08c00",
			"backgroundColor": "#ffc9c9",
			"width": 15.598219043002732,
			"height": 15.598219043002732,
			"seed": 1664466708,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false
		},
		{
			"type": "line",
			"version": 168,
			"versionNonce": 1573385132,
			"isDeleted": false,
			"id": "2FL42ciSZlWfa3-Y_axF3",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 18.96316275622337,
			"y": -56.33000900535171,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 84.15948455196997,
			"seed": 674514068,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-84.15948455196997
				]
			]
		},
		{
			"type": "arrow",
			"version": 1850,
			"versionNonce": 912563756,
			"isDeleted": false,
			"id": "TVWb4imJpkWKd6a2wttLo",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 37.73558863425096,
			"y": 24.612182586564927,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 0.035574515394039,
			"height": 195.72141446170207,
			"seed": 552036908,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "nXi9JHhG",
				"focus": 0.40828036590427946,
				"gap": 14.68936180744845
			},
			"endBinding": {
				"elementId": "ITGOIHXR",
				"focus": 0.9279589038216995,
				"gap": 15.440429687500114
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					0.035574515394039,
					-195.72141446170207
				]
			]
		},
		{
			"type": "line",
			"version": 451,
			"versionNonce": 1180478100,
			"isDeleted": false,
			"id": "xLNE6O5bhZlE1NDzoTAuW",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -144.22558505409904,
			"y": -241.01336674834113,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 275.35171106293114,
			"height": 313.87825106727945,
			"seed": 1620823852,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					215.2955604635034,
					296.88123074346214
				],
				[
					275.35171106293114,
					-16.99702032381731
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 710,
			"versionNonce": 1087563284,
			"isDeleted": false,
			"id": "GAyNm22gx2CC83yXa2cMz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -251.27294252690047,
			"y": 94.80879860567978,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 360.583984375,
			"seed": 2033256876,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151816167,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-360.583984375
				]
			]
		},
		{
			"type": "arrow",
			"version": 695,
			"versionNonce": 188023700,
			"isDeleted": false,
			"id": "pxtyqPBibz-RZNfLQHJG2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -251.2729272681114,
			"y": 93.28737526583603,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 390.2522735595703,
			"height": 149.86297607421875,
			"seed": 1882911788,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151816167,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					390.2522735595703,
					149.86297607421875
				]
			]
		},
		{
			"type": "arrow",
			"version": 679,
			"versionNonce": 1316450580,
			"isDeleted": false,
			"id": "wtLNwq0QHMV59Znelb-i3",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -250.5122308569786,
			"y": 94.80879860567978,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 429.8100280761719,
			"height": 159.75238037109375,
			"seed": 1940098732,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151816167,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					429.8100280761719,
					-159.75238037109375
				]
			]
		},
		{
			"type": "text",
			"version": 597,
			"versionNonce": 1052382612,
			"isDeleted": false,
			"id": "OuJR8qzb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 35.5503606957559,
			"y": 76.68024363152279,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 23.088592529296875,
			"height": 56.82167020853201,
			"seed": 1679087916,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 45.45733616682561,
			"fontFamily": 1,
			"text": "b",
			"rawText": "b",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "b",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1167,
			"versionNonce": 835370412,
			"isDeleted": false,
			"id": "8LC-Marm88VEAY6IUnqCz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -142.45943544682234,
			"y": 93.28743630099228,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 2.842170943040401e-14,
			"height": 336.24078369140625,
			"seed": 2096681900,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "2YL3vkEB",
				"focus": -2.3950752117013714,
				"gap": 12.932342529296875
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					2.842170943040401e-14,
					-336.24078369140625
				]
			]
		},
		{
			"type": "arrow",
			"version": 1600,
			"versionNonce": 864699156,
			"isDeleted": false,
			"id": "oHza5r2Tz700SQQ6SGcze",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 73.5866155785684,
			"y": 190.66030007052353,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 7.105427357601002e-15,
			"height": 133.8876953125,
			"seed": 2088336940,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "iGCoONQL",
				"focus": 1.194139631697295,
				"gap": 8.300558976920854
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					7.105427357601002e-15,
					-133.8876953125
				]
			]
		},
		{
			"type": "arrow",
			"version": 1254,
			"versionNonce": 1400776748,
			"isDeleted": false,
			"id": "eY52DvxAxeG-k5JHqVOw5",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 129.11963559809965,
			"y": 39.27590065646109,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 295.2172333096487,
			"seed": 1266735276,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "ajIPCVDS",
				"focus": 1.3464364244796498,
				"gap": 12.721138000488281
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-295.2172333096487
				]
			]
		},
		{
			"type": "text",
			"version": 569,
			"versionNonce": 1224261780,
			"isDeleted": false,
			"id": "2YL3vkEB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -129.52709291752547,
			"y": 101.65541725802353,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 18.539993286132812,
			"height": 25,
			"seed": 632873772,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "8LC-Marm88VEAY6IUnqCz",
					"type": "arrow"
				}
			],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A1",
			"rawText": "A1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A1",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 653,
			"versionNonce": 1885356716,
			"isDeleted": false,
			"id": "YTqQwyLw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 94.72930814448637,
			"y": 180.20027809786728,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 27.3599853515625,
			"height": 25,
			"seed": 1099668908,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A2",
			"rawText": "A2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A2",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 563,
			"versionNonce": 1992003092,
			"isDeleted": false,
			"id": "ucKQtBGi",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -138.65581606205672,
			"y": -284.0325405056484,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 57.03996276855469,
			"height": 25,
			"seed": 2015561772,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A1,C1)",
			"rawText": "(A1,C1)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A1,C1)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 636,
			"versionNonce": 253431084,
			"isDeleted": false,
			"id": "ajIPCVDS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 141.84077359858793,
			"y": -282.3080226833828,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 73.43995666503906,
			"height": 25,
			"seed": 1835606700,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "eY52DvxAxeG-k5JHqVOw5",
					"type": "arrow"
				}
			],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A3,C3)",
			"rawText": "(A3,C3)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A3,C3)",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 536,
			"versionNonce": 1304996756,
			"isDeleted": false,
			"id": "iMJ2GzhohK6mmMFrwMuax",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 11.1187443566821,
			"y": -153.41042366390127,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffc9c9",
			"width": 15.598219043002732,
			"height": 15.598219043002732,
			"seed": 940043564,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 330,
			"versionNonce": 1347819796,
			"isDeleted": false,
			"id": "3pRldLkS",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -287.333624278421,
			"y": -283.3793055232469,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 16.0159912109375,
			"height": 35,
			"seed": 1438093868,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "z",
			"rawText": "z",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "z",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 1047,
			"versionNonce": 444772908,
			"isDeleted": false,
			"id": "iGCoONQL",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 80.83578427168129,
			"y": 65.07316373494439,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 74.67994689941406,
			"height": 25,
			"seed": 2078043308,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "oHza5r2Tz700SQQ6SGcze",
					"type": "arrow"
				}
			],
			"updated": 1714151811604,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A2,C2)",
			"rawText": "(A2,C2)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A2,C2)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 996,
			"versionNonce": 1239013012,
			"isDeleted": false,
			"id": "nXi9JHhG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 27.146967801684468,
			"y": 39.301544394013376,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 15.098388671875,
			"height": 14.570647687037667,
			"seed": 1837233964,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "TVWb4imJpkWKd6a2wttLo",
					"type": "arrow"
				}
			],
			"updated": 1714151811605,
			"link": null,
			"locked": false,
			"fontSize": 11.656518149630134,
			"fontFamily": 1,
			"text": "A4",
			"rawText": "A4",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A4",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1545,
			"versionNonce": 977505452,
			"isDeleted": false,
			"id": "Ikt_L7f6LnDINbh6Kt0zE",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 19.109982646383003,
			"y": -147.8246916812738,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 1.0658141036401503e-14,
			"height": 156.93742309702384,
			"seed": 1460810156,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811605,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.0658141036401503e-14,
					-156.93742309702384
				]
			]
		},
		{
			"type": "line",
			"version": 1193,
			"versionNonce": 1043419564,
			"isDeleted": false,
			"id": "WBLzSOJ38A96n3MNdd-Um",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -142.79584171418207,
			"y": -244.1729611352348,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 215.90680010359185,
			"height": 303.6047593420656,
			"seed": 338903828,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151811605,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					215.90680010359185,
					303.6047593420656
				],
				[
					179.49924279967288,
					73.48050220217601
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 771,
			"versionNonce": 256312980,
			"isDeleted": false,
			"id": "BgdawluR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 252.24924964840966,
			"y": -69.8450660389455,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 101.67155456542969,
			"height": 55.44146764114398,
			"seed": 1829108652,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714152092516,
			"link": null,
			"locked": false,
			"fontSize": 22.176587056457592,
			"fontFamily": 1,
			"text": "4 enters\nthe basis",
			"rawText": "4 enters\nthe basis",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4 enters\nthe basis",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 50,
			"versionNonce": 1550516884,
			"isDeleted": false,
			"id": "0cJcHBDbXWGvGWxn922hj",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 38.11937083793066,
			"y": -172.20314045720522,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 84.96064461556693,
			"height": 79.45962215773272,
			"seed": 442666924,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151811605,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					84.96064461556693,
					-79.45962215773272
				]
			]
		},
		{
			"type": "text",
			"version": 740,
			"versionNonce": 647335956,
			"isDeleted": false,
			"id": "ITGOIHXR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 35.19207669917387,
			"y": -211.54966156263725,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 71.79994201660156,
			"height": 25,
			"seed": 55657644,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "TVWb4imJpkWKd6a2wttLo",
					"type": "arrow"
				}
			],
			"updated": 1714151811605,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A4,C4)",
			"rawText": "(A4,C4)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A4,C4)",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 405,
			"versionNonce": 78480812,
			"isDeleted": false,
			"id": "vqGLOI3MkwgKcd5d0128Z",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "dotted",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -369.83860926186264,
			"y": 106.23826968944532,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 345.59591580751623,
			"height": 156.38120574268368,
			"seed": 1477342740,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151915786,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "fXHShpFv",
				"focus": -0.6615964057561344,
				"gap": 23.374813572504706
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "triangle",
			"points": [
				[
					0,
					0
				],
				[
					147.42312782926547,
					-109.19723963720664
				],
				[
					345.59591580751623,
					-156.38120574268368
				]
			]
		},
		{
			"type": "text",
			"version": 371,
			"versionNonce": 1626852140,
			"isDeleted": false,
			"id": "fXHShpFv",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "dotted",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -471.519670984976,
			"y": 129.61308326195,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 245.6798095703125,
			"height": 125,
			"seed": 2136686380,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "vqGLOI3MkwgKcd5d0128Z",
					"type": "arrow"
				}
			],
			"updated": 1714151915786,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "The simplex generated\nby 1,2,4 intersect the\nrequirement line first:\n3 is left out and leaves\nthe basis",
			"rawText": "The simplex generated\nby 1,2,4 intersect the\nrequirement line first:\n3 is left out and leaves\nthe basis",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "The simplex generated\nby 1,2,4 intersect the\nrequirement line first:\n3 is left out and leaves\nthe basis",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 170,
			"versionNonce": 659624980,
			"isDeleted": false,
			"id": "RO28bLOZ",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "dotted",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 210.3651020043667,
			"y": -218.0380346928695,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 185.43984985351562,
			"height": 100,
			"seed": 694997524,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714152092516,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "We start from the\nbasic simplex \nassociated to\n1,2,3",
			"rawText": "We start from the\nbasic simplex \nassociated to\n1,2,3",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "We start from the\nbasic simplex \nassociated to\n1,2,3",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1971c2",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 4,
		"currentItemStrokeStyle": "dotted",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": "dot",
		"currentItemEndArrowhead": "triangle",
		"scrollX": 615.1966055899476,
		"scrollY": 329.52254747961774,
		"zoom": {
			"value": 1.4500000000000002
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		},
		"objectsSnapModeEnabled": false
	},
	"files": {}
}
```
%%
---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
P ^Yu8fhBa6

P ^dzjd0QVB

random
peturbation ^AvOv1jZD

polyhedron with degenerate points ^uzMn6Lg6

P ^ZhAKODYV

polyhedron without degenerate points ^vxnWp4dU

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.6",
	"elements": [
		{
			"id": "gZlLQ_JBek2YpSmtg0Gyw",
			"type": "line",
			"x": 313.97079561927626,
			"y": -18.402249627771198,
			"width": 223.42531642300872,
			"height": 95.97309771803353,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1064665364,
			"version": 270,
			"versionNonce": 1835872020,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077528496,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					59.11937744852537,
					-89.06303360263759
				],
				[
					115.93549016671194,
					-95.97309771803354
				],
				[
					223.42531642300872,
					-9.981196224339172
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0,
				3.071132108943175
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "OJ3R55OuxR5p6xv0QjUJz",
			"type": "line",
			"x": -174.22054838884333,
			"y": -15.94516416701947,
			"width": 227.24429113793468,
			"height": 119.70907647638063,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 416492436,
			"version": 260,
			"versionNonce": 1900354476,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077336170,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					78.11533096043013,
					-111.59322804222404
				],
				[
					227.24429113793468,
					8.11584843415659
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				-1.0145033451674408,
				0
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "lZVKXdTGz6KcOISPZMnid",
			"type": "line",
			"x": -191.140380859375,
			"y": 6.58636474609375,
			"width": 163.8887939453125,
			"height": 235.41665649414062,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 681643796,
			"version": 137,
			"versionNonce": 306535084,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077261420,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					163.8887939453125,
					-235.41665649414062
				]
			],
			"lastCommittedPoint": [
				163.8887939453125,
				-235.41665649414062
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "phFIjeSGnVCvTQXK4Tm2q",
			"type": "line",
			"x": 67.19287109375,
			"y": 4.5030517578125,
			"width": 267.361083984375,
			"height": 215.27780151367188,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 733772564,
			"version": 101,
			"versionNonce": 592705044,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077261420,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-267.361083984375,
					-215.27780151367188
				]
			],
			"lastCommittedPoint": [
				-267.361083984375,
				-215.27780151367188
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "line",
			"version": 252,
			"versionNonce": 2040227476,
			"isDeleted": false,
			"id": "7JksHRKG0mTnStGhFzOVW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 304.1929581808068,
			"y": -5.267191458105799,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 163.8887939453125,
			"height": 235.41665649414062,
			"seed": 484551316,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714077523426,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					163.8887939453125,
					-235.41665649414062
				]
			]
		},
		{
			"type": "line",
			"version": 213,
			"versionNonce": 1980716692,
			"isDeleted": false,
			"id": "hVitC97KbNLlh_xu38DFw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 561.7584102364222,
			"y": -7.350504446387049,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 267.361083984375,
			"height": 215.27780151367188,
			"seed": 1912027156,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714077387879,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-267.361083984375,
					-215.27780151367188
				]
			]
		},
		{
			"id": "_n81DOnu84_7abpmuvwH0",
			"type": "line",
			"x": -218.8578039402962,
			"y": -111.30665075913441,
			"width": 247.5340013869178,
			"height": 34.49244500875716,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 2037071508,
			"version": 140,
			"versionNonce": 1298677164,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077277319,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					247.5340013869178,
					-34.49244500875716
				]
			],
			"lastCommittedPoint": [
				247.5340013869178,
				-34.49244500875716
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "A9WHI24qAYZPKbXr6e1-M",
			"type": "freedraw",
			"x": -86.81292307368773,
			"y": -64.46936622410885,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1874526868,
			"version": 3,
			"versionNonce": 923413292,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077344121,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		},
		{
			"id": "Yu8fhBa6",
			"type": "text",
			"x": -107.54311541987556,
			"y": -60.6304342176561,
			"width": 13.219985961914062,
			"height": 25,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1147640596,
			"version": 2,
			"versionNonce": 501181100,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077350219,
			"link": null,
			"locked": false,
			"text": "P",
			"rawText": "P",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "P",
			"lineHeight": 1.25
		},
		{
			"id": "dzjd0QVB",
			"type": "text",
			"x": -96.79405181693198,
			"y": -56.55573605025853,
			"width": 22.43344976983855,
			"height": 42.42336155739477,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2118904852,
			"version": 78,
			"versionNonce": 1240400276,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077381891,
			"link": null,
			"locked": false,
			"text": "P",
			"rawText": "P",
			"fontSize": 33.938689245915825,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "P",
			"lineHeight": 1.25
		},
		{
			"id": "uF0M_EYwLMpyXN_JXbxOu",
			"type": "arrow",
			"x": 61.369487977475046,
			"y": -145.85453581384127,
			"width": 198.08845965395352,
			"height": 0.7677998975095193,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 179421588,
			"version": 89,
			"versionNonce": 1640484500,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "AvOv1jZD"
				}
			],
			"updated": 1714077557749,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					198.08845965395352,
					0.7677998975095193
				]
			],
			"lastCommittedPoint": [
				198.08845965395352,
				0.7677998975095193
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "triangle_outline"
		},
		{
			"id": "AvOv1jZD",
			"type": "text",
			"x": 115.74974685718618,
			"y": -165.4706358650865,
			"width": 89.32794189453125,
			"height": 40,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1720717204,
			"version": 25,
			"versionNonce": 2130840980,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077543805,
			"link": null,
			"locked": false,
			"text": "random\npeturbation",
			"rawText": "random\npeturbation",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "uF0M_EYwLMpyXN_JXbxOu",
			"originalText": "random\npeturbation",
			"lineHeight": 1.25
		},
		{
			"id": "uzMn6Lg6",
			"type": "text",
			"x": -240.37006836960717,
			"y": 29.771531391434223,
			"width": 329.09967041015625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 846472364,
			"version": 109,
			"versionNonce": 298564908,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1714077441321,
			"link": null,
			"locked": false,
			"text": "polyhedron with degenerate points",
			"rawText": "polyhedron with degenerate points",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "polyhedron with degenerate points",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 213,
			"versionNonce": 1417663764,
			"isDeleted": false,
			"id": "MUbp8tV-zUGefADZbtyqB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 277.89422340563317,
			"y": -93.9438904934961,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 247.5340013869178,
			"height": 34.49244500875716,
			"seed": 325306644,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714077458652,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					247.5340013869178,
					-34.49244500875716
				]
			]
		},
		{
			"type": "text",
			"version": 144,
			"versionNonce": 1147692308,
			"isDeleted": false,
			"id": "ZhAKODYV",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 391.63288094798065,
			"y": -76.55969488938895,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 22.43344976983855,
			"height": 42.42336155739477,
			"seed": 1724375340,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714077540420,
			"link": null,
			"locked": false,
			"fontSize": 33.938689245915825,
			"fontFamily": 1,
			"text": "P",
			"rawText": "P",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "P",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 194,
			"versionNonce": 472128916,
			"isDeleted": false,
			"id": "vxnWp4dU",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 245.33265033589373,
			"y": 22.136338880300627,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 362.83966064453125,
			"height": 25,
			"seed": 127395604,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714077570705,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "polyhedron without degenerate points",
			"rawText": "polyhedron without degenerate points",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "polyhedron without degenerate points",
			"lineHeight": 1.25
		},
		{
			"id": "PzKa3lB3",
			"type": "text",
			"x": 135.07692851649165,
			"y": -422.2570835594046,
			"width": 45.299957275390625,
			"height": 25,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 576623276,
			"version": 6,
			"versionNonce": 1869963924,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077610458,
			"link": null,
			"locked": false,
			"text": "asdf",
			"rawText": "asdf",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "asdf",
			"lineHeight": 1.25
		},
		{
			"id": "ofN-DYKB7kQBZJmKdz_YM",
			"type": "line",
			"x": 129.69287109375,
			"y": 44.78082275390625,
			"width": 2.6116324377153433,
			"height": 50.86715195770648,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1765599788,
			"version": 55,
			"versionNonce": 1688020268,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077261420,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2.6116324377153433,
					50.86715195770648
				]
			],
			"lastCommittedPoint": [
				-2.6116324377153433,
				50.86715195770648
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "17XgNiHL",
			"type": "text",
			"x": 27.587102260194797,
			"y": -49.881438095807425,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1652884372,
			"version": 2,
			"versionNonce": 1534253612,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077356017,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "ySD0xvFM",
			"type": "text",
			"x": -106.77531552236599,
			"y": 66.05411955199929,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 939867156,
			"version": 2,
			"versionNonce": 473069996,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077359041,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "GBfse806",
			"type": "text",
			"x": 86.70647970872011,
			"y": -178.86929033634902,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 655017364,
			"version": 2,
			"versionNonce": 2064021548,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077362427,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "8khobMOZ",
			"type": "text",
			"x": 94.07730473993564,
			"y": -440.03897625959894,
			"width": 87.49990844726562,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 472550444,
			"version": 11,
			"versionNonce": 1375939116,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077372299,
			"link": null,
			"locked": false,
			"text": "asdfdfdf",
			"rawText": "asdfdfdf",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "asdfdfdf",
			"lineHeight": 1.25
		},
		{
			"id": "gKg87a6gtNpYlA-5MO4dT",
			"type": "line",
			"x": 293.2406032730885,
			"y": -95.18075479463579,
			"width": 274.09916492330876,
			"height": 14.587894387753977,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1288135212,
			"version": 62,
			"versionNonce": 2020949524,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077452271,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					274.09916492330876,
					-14.587894387753977
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "lfIYR7NNvTQkYsMamLpgx",
			"type": "line",
			"x": 256.38701796577004,
			"y": 109.81783645580833,
			"width": 211.90858788474532,
			"height": 93.66979924714735,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1089773588,
			"version": 493,
			"versionNonce": 1902912812,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714077492074,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					57.58377765350622,
					-85.99196897478936
				],
				[
					112.86429057667374,
					-93.66979924714735
				],
				[
					211.90858788474532,
					-10.748996121848677
				],
				[
					9.213328845734736,
					-10.748996121848677
				],
				[
					31.479120986941552,
					-49.13824870528106
				],
				[
					58.351577551015794,
					-83.6885692822608
				],
				[
					119.00655479456009,
					-90.59863339765673
				],
				[
					206.53412356436843,
					-12.284595916867715
				],
				[
					12.284528435772813,
					-11.516796019358253
				]
			],
			"lastCommittedPoint": [
				12.284528435772813,
				-11.516796019358253
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "#b2f2bb",
		"currentItemFillStyle": "cross-hatch",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "triangle_outline",
		"scrollX": 329.83791765289016,
		"scrollY": 662.1082692288937,
		"zoom": {
			"value": 0.8268651471674989
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		},
		"objectsSnapModeEnabled": false
	},
	"files": {}
}
```
%%
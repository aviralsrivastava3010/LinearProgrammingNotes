---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
b ^l4e7xL9h

A1 ^HB1usllT

A2 ^mHJSmumz

A3 ^0DxPvhbd

A4 ^pE8uqPL6

(A1,C1) ^Hq8WvNMm

(A3,C3) ^tP6ZSNpD

(A4,C4) ^Nwbj65Ao

z ^f4tZKXjJ

Convex Hull 
of (Ai,Ci) ^B7XJmpqn

(A2,C2) ^3MRCK2v7

# Embedded files
b64da49dc60555a7c05b504c0450635aa800b934: $$\color{green}
(b,z^{*})$$

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.6",
	"elements": [
		{
			"type": "arrow",
			"version": 215,
			"versionNonce": 220465006,
			"isDeleted": false,
			"id": "sy81jib703ib_QZo6AF2f",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.19543203359126,
			"y": 2.605391927728874,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 360.583984375,
			"seed": 1795308146,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591269,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-360.583984375
				]
			]
		},
		{
			"type": "arrow",
			"version": 201,
			"versionNonce": 1284828786,
			"isDeleted": false,
			"id": "CN5xMVp1ATlYI_DM_SRNz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.1954167748022,
			"y": 1.0839685878851242,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 390.2522735595703,
			"height": 149.86297607421875,
			"seed": 1534467122,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591269,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					390.2522735595703,
					149.86297607421875
				]
			]
		},
		{
			"type": "arrow",
			"version": 185,
			"versionNonce": 642574766,
			"isDeleted": false,
			"id": "-a_R4l4w5rBpAQPI1l8uK",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -151.43472036366938,
			"y": 2.605391927728874,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 429.8100280761719,
			"height": 159.75238037109375,
			"seed": 1585956338,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					429.8100280761719,
					-159.75238037109375
				]
			]
		},
		{
			"type": "arrow",
			"version": 456,
			"versionNonce": 1620201522,
			"isDeleted": false,
			"id": "zrvANsN-wPfPRVh4wulPA",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 118.65259042734624,
			"y": 4.126937337885124,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 169.40244483171944,
			"seed": 1832794034,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "l4e7xL9h",
				"focus": -2.383824565438355,
				"gap": 15.97528076171875
			},
			"endBinding": {
				"elementId": "yyl3daEBZtMRq0pdhN9AI",
				"focus": 0.0822199637871264,
				"gap": 1.714296015031918
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-169.40244483171944
				]
			]
		},
		{
			"type": "text",
			"version": 148,
			"versionNonce": 1688175598,
			"isDeleted": false,
			"id": "l4e7xL9h",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 134.627871189065,
			"y": -15.523163046428124,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 23.088592529296875,
			"height": 56.82167020853201,
			"seed": 1203794290,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "zrvANsN-wPfPRVh4wulPA",
					"type": "arrow"
				}
			],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 45.45733616682561,
			"fontFamily": 1,
			"text": "b",
			"rawText": "b",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "b",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 345,
			"versionNonce": 1509784050,
			"isDeleted": false,
			"id": "95XAu8gTT3cZV1WmBuh4S",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -43.381924953513135,
			"y": 1.0840296230413742,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 2.842170943040401e-14,
			"height": 336.24078369140625,
			"seed": 290812722,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "HB1usllT",
				"focus": -2.395075211701372,
				"gap": 12.932342529296875
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					2.842170943040401e-14,
					-336.24078369140625
				]
			]
		},
		{
			"type": "arrow",
			"version": 164,
			"versionNonce": 1780799022,
			"isDeleted": false,
			"id": "t56bELNKGVNyqfqncokep",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 136.91003671640874,
			"y": -81.07441764258363,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 0.05417625971858797,
			"height": 184.8563232421875,
			"seed": 1818508530,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "Nwbj65Ao",
				"focus": 0.9280922907956247,
				"gap": 15.4404296875
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					-0.05417625971858797,
					-184.8563232421875
				]
			]
		},
		{
			"type": "arrow",
			"version": 592,
			"versionNonce": 1474128818,
			"isDeleted": false,
			"id": "zI__IA33dPfZbwvSAjKaP",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 172.6641260718775,
			"y": 98.45689339257262,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 7.105427357601002e-15,
			"height": 133.8876953125,
			"seed": 376688306,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					7.105427357601002e-15,
					-133.8876953125
				]
			]
		},
		{
			"type": "arrow",
			"version": 177,
			"versionNonce": 801497198,
			"isDeleted": false,
			"id": "IRQmpzWONZWSHrUx4Jyxw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 225.5560715469895,
			"y": -49.523889317558655,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 2.842170943040401e-14,
			"height": 294.41200256836254,
			"seed": 69937266,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "tP6ZSNpD",
				"focus": 1.4183611549493367,
				"gap": 15.362212544907493
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					2.842170943040401e-14,
					-294.41200256836254
				]
			]
		},
		{
			"type": "text",
			"version": 123,
			"versionNonce": 5842290,
			"isDeleted": false,
			"id": "HB1usllT",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -30.44958242421626,
			"y": 9.452010580072624,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 18.539993286132812,
			"height": 25,
			"seed": 2102479410,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "95XAu8gTT3cZV1WmBuh4S",
					"type": "arrow"
				}
			],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A1",
			"rawText": "A1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A1",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 207,
			"versionNonce": 692822702,
			"isDeleted": false,
			"id": "mHJSmumz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 193.80681863779546,
			"y": 87.99687141991637,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 27.3599853515625,
			"height": 25,
			"seed": 952972274,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A2",
			"rawText": "A2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A2",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 294,
			"versionNonce": 481797938,
			"isDeleted": false,
			"id": "0DxPvhbd",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 241.93275623742795,
			"y": -63.36811227539215,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 26.739990234375,
			"height": 25,
			"seed": 2024138162,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A3",
			"rawText": "A3",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A3",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 279,
			"versionNonce": 1767841006,
			"isDeleted": false,
			"id": "pE8uqPL6",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 154.7947413062525,
			"y": -94.23414664648988,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 25.91998291015625,
			"height": 25,
			"seed": 637689714,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A4",
			"rawText": "A4",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A4",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 118,
			"versionNonce": 895480050,
			"isDeleted": false,
			"id": "Hq8WvNMm",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -39.57830556874751,
			"y": -376.23594718359925,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 57.03996276855469,
			"height": 25,
			"seed": 1831444786,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A1,C1)",
			"rawText": "(A1,C1)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A1,C1)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 188,
			"versionNonce": 1166626606,
			"isDeleted": false,
			"id": "tP6ZSNpD",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 240.91828409189702,
			"y": -374.5114293613336,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 73.43995666503906,
			"height": 25,
			"seed": 1501735666,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "IRQmpzWONZWSHrUx4Jyxw",
					"type": "arrow"
				}
			],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A3,C3)",
			"rawText": "(A3,C3)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A3,C3)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 255,
			"versionNonce": 769196722,
			"isDeleted": false,
			"id": "Nwbj65Ao",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 134.26958719248296,
			"y": -306.3711705722711,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 71.79994201660156,
			"height": 25,
			"seed": 1905556658,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "t56bELNKGVNyqfqncokep",
					"type": "arrow"
				}
			],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A4,C4)",
			"rawText": "(A4,C4)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A4,C4)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 49,
			"versionNonce": 2050177390,
			"isDeleted": false,
			"id": "f4tZKXjJ",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -186.08572641367437,
			"y": -369.9199322685198,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 16.0159912109375,
			"height": 35,
			"seed": 71606770,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "z",
			"rawText": "z",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "z",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 221,
			"versionNonce": 1623841906,
			"isDeleted": false,
			"id": "rWm1vhxuOdlvT2d6pwMk-",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -41.530056713824195,
			"y": -337.2035905257836,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 265.3159501139976,
			"height": 313.0931270158121,
			"seed": 2116520110,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					213.47258104081172,
					302.92774853682215
				],
				[
					265.3159501139976,
					-10.165378478989965
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 201,
			"versionNonce": 1319492526,
			"isDeleted": false,
			"id": "6M5Swg8Agn9SI9ovf02cW",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 228.8686030295559,
			"y": -343.3028583929528,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 268.3655840475821,
			"height": 78.27327155900917,
			"seed": 1428403502,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-94.53787712539315,
					78.27327155900917
				],
				[
					-268.3655840475821,
					8.132262003416713
				]
			]
		},
		{
			"type": "text",
			"version": 89,
			"versionNonce": 1006206514,
			"isDeleted": false,
			"id": "B7XJmpqn",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 270.54655284397666,
			"y": -279.17840707514694,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 166.37594604492188,
			"height": 70,
			"seed": 1813189230,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Convex Hull \nof (Ai,Ci)",
			"rawText": "Convex Hull \nof (Ai,Ci)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Convex Hull \nof (Ai,Ci)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 262,
			"versionNonce": 956970478,
			"isDeleted": false,
			"id": "3MRCK2v7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 191.39697187936656,
			"y": -15.476875116364624,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 74.67994689941406,
			"height": 25,
			"seed": 1733347826,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713902591270,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A2,C2)",
			"rawText": "(A2,C2)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A2,C2)",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 237,
			"versionNonce": 564029484,
			"isDeleted": false,
			"id": "e1k7HasVijHc3BoffZuT8",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 173.44357962863495,
			"y": -31.725861045628335,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 214.1302038122107,
			"height": 304.85380949797445,
			"seed": 1968850546,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714078094618,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-36.06400101273148,
					-232.72573965567125
				],
				[
					-214.1302038122107,
					-304.85380949797445
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 549,
			"versionNonce": 40454190,
			"isDeleted": false,
			"id": "yydzT9aOIO9J-oswnPn_9",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 118.62528354935256,
			"y": -167.60128522265052,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 199.97091868683776,
			"seed": 1442276654,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713902591271,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "yyl3daEBZtMRq0pdhN9AI",
				"focus": -0.08545058730397223,
				"gap": 4.037195078391948
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-199.97091868683776
				]
			]
		},
		{
			"type": "ellipse",
			"version": 88,
			"versionNonce": 701569458,
			"isDeleted": false,
			"id": "yyl3daEBZtMRq0pdhN9AI",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 110.89504537660957,
			"y": -163.58499190500328,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#ffc9c9",
			"width": 16.90502025462962,
			"height": 16.90502025462962,
			"seed": 1738903982,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "zrvANsN-wPfPRVh4wulPA",
					"type": "arrow"
				},
				{
					"id": "yydzT9aOIO9J-oswnPn_9",
					"type": "arrow"
				}
			],
			"updated": 1713902591271,
			"link": null,
			"locked": false
		},
		{
			"type": "image",
			"version": 325,
			"versionNonce": 1566398228,
			"isDeleted": false,
			"id": "t3uJBpPL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 17.92098553537747,
			"y": -143.87162925740597,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 74.21127856219262,
			"height": 28.96049895109956,
			"seed": 27872,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714078044744,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "b64da49dc60555a7c05b504c0450635aa800b934",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "d2IOLFBQ",
			"type": "text",
			"x": 85.84557657809432,
			"y": -243.18353751200226,
			"width": 14,
			"height": 35,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 155216532,
			"version": 2,
			"versionNonce": 613172012,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1714078088224,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 28,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "#b2f2bb",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 4,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 202.8526755863062,
		"scrollY": 451.60868963039167,
		"zoom": {
			"value": 1.3
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		},
		"objectsSnapModeEnabled": false
	},
	"files": {}
}
```
%%
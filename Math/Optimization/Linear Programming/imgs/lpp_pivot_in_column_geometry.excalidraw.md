---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
(A4,C4) ^24jSqaEY

b ^kRsLLXv7

A1 ^c4WID9CB

A2 ^x58jlv8o

(A1,C1) ^eZGnJAHs

(A3,C3) ^qCfLXL7c

(b,z) ^a5fb3oRN

z ^MIUZ6gI0

(A2,C2) ^qxhSyiTd

A4 ^CiKKnW3B

(A4,C4) ^NjR6Z4c9

b ^QTE6NKBX

A1 ^9jAPfy2H

A2 ^4hxp9smZ

A3 ^oKLwdqIW

(A3,C3) ^bUHtca8n

(b,z) ^lFaMtxAp

z ^tC219JKW

(A2,C2) ^gDJdSfc0

A4 ^OtGw5FrE

(A1,C1) ^nWTpXN9R

3 exits
4 enters ^5vI4zCbj

By pivoting to
a lower costing
colum we improve z ^OiIGhcTp

the simplex
pivots around
the m-1 simplex
that acts as
an anchor ^YHodX06X

the simplex
pivots around
the m-1 simplex
that acts as
an anchor ^R6EDW1Ok

the simplex
pivots around
the m-1 simplex
that acts as
an anchor ^uIqHG9k6

the simplex
pivots around
the m-1 simplex
that acts as
an anchor ^BuQg68lL

the simplex
pivots around
the m-1 simplex
that acts as
an anchor ^S583Ulr3

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.6",
	"elements": [
		{
			"type": "line",
			"version": 423,
			"versionNonce": 1146679980,
			"isDeleted": false,
			"id": "d8qiW2kpXXn8k-u0MnuaJ",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -212.6634495359434,
			"y": -21.69315149735445,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 270.81916624894325,
			"height": 151.84002398222196,
			"seed": 1135304306,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					214.16240153136937,
					95.18335017919654
				],
				[
					270.81916624894325,
					-56.65667380302543
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 1205,
			"versionNonce": 2145771028,
			"isDeleted": false,
			"id": "MUiwUzKRK3BMSnFKTZN_g",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -52.262215886934996,
			"y": -20.89032495584746,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 1.0432641610067321,
			"height": 224.81725308417413,
			"seed": 190496818,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "kRsLLXv7",
				"focus": -2.360389561731759,
				"gap": 15.97528076171875
			},
			"endBinding": {
				"elementId": "a5fb3oRN",
				"focus": -1.1830316915168086,
				"gap": 5.497496897449537
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.0432641610067321,
					-224.81725308417413
				]
			]
		},
		{
			"type": "arrow",
			"version": 1605,
			"versionNonce": 1081982252,
			"isDeleted": false,
			"id": "8mcWa_3xAt9m5Z5bFShK7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -34.004769597872496,
			"y": -106.09167993631621,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 0.05417625971858797,
			"height": 184.8563232421875,
			"seed": 1916266994,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "cHrgALmFaVVHdFIdCcNFd",
				"focus": 0.41523580009380195,
				"gap": 10.446928123189682
			},
			"endBinding": {
				"elementId": "lbE33VhrI3AfRCBPqaHxN",
				"focus": 0.9280922907956247,
				"gap": 15.4404296875
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					-0.05417625971858797,
					-184.8563232421875
				]
			]
		},
		{
			"type": "text",
			"version": 648,
			"versionNonce": 1562966932,
			"isDeleted": false,
			"id": "24jSqaEY",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -36.64521912179828,
			"y": -331.38843286600377,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 71.79994201660156,
			"height": 25,
			"seed": 1598675890,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "8mcWa_3xAt9m5Z5bFShK7",
					"type": "arrow"
				}
			],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A4,C4)",
			"rawText": "(A4,C4)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A4,C4)",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 392,
			"versionNonce": 581253036,
			"isDeleted": false,
			"id": "TKwusd2PZJKfjEikmgC61",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 70,
			"angle": 0,
			"x": -216.0628808750712,
			"y": -358.2340357200246,
			"strokeColor": "#1971c2",
			"backgroundColor": "#a5d8ff",
			"width": 275.35171106293114,
			"height": 313.87825106727945,
			"seed": 1085773170,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					215.2955604635034,
					296.88123074346214
				],
				[
					275.35171106293114,
					-16.99702032381731
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 608,
			"versionNonce": 791439636,
			"isDeleted": false,
			"id": "80qgdunWJsvFgv53AbJPR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -323.11023834787255,
			"y": -22.41187036600371,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 360.583984375,
			"seed": 667424562,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-360.583984375
				]
			]
		},
		{
			"type": "arrow",
			"version": 593,
			"versionNonce": 1996055084,
			"isDeleted": false,
			"id": "x3AgwaHFwAw09PlzQNnrS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -323.1102230890835,
			"y": -23.93329370584746,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 390.2522735595703,
			"height": 149.86297607421875,
			"seed": 1692557554,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					390.2522735595703,
					149.86297607421875
				]
			]
		},
		{
			"type": "arrow",
			"version": 577,
			"versionNonce": 426124948,
			"isDeleted": false,
			"id": "ExZxmSqVniETqgdUGYcGU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -322.3495266779507,
			"y": -22.41187036600371,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 429.8100280761719,
			"height": 159.75238037109375,
			"seed": 596472498,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					429.8100280761719,
					-159.75238037109375
				]
			]
		},
		{
			"type": "text",
			"version": 540,
			"versionNonce": 878984364,
			"isDeleted": false,
			"id": "kRsLLXv7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -36.286935125216246,
			"y": -40.54042534016071,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 23.088592529296875,
			"height": 56.82167020853201,
			"seed": 108022898,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "MUiwUzKRK3BMSnFKTZN_g",
					"type": "arrow"
				}
			],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"fontSize": 45.45733616682561,
			"fontFamily": 1,
			"text": "b",
			"rawText": "b",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "b",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1108,
			"versionNonce": 1619801108,
			"isDeleted": false,
			"id": "NNd79D1hMMXzbibR3sZv4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 50,
			"angle": 0,
			"x": -214.29673126779443,
			"y": -23.93323267069121,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 2.842170943040401e-14,
			"height": 336.24078369140625,
			"seed": 1129442866,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "xd1DnSnAlObi3SRUtV2D8",
				"focus": -2.395075211701372,
				"gap": 12.932342529296875
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					2.842170943040401e-14,
					-336.24078369140625
				]
			]
		},
		{
			"type": "arrow",
			"version": 1545,
			"versionNonce": 1687144236,
			"isDeleted": false,
			"id": "yhrBMGoxTd2AbWbJFbbBf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 1.749319757596254,
			"y": 73.43963109884004,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 7.105427357601002e-15,
			"height": 133.8876953125,
			"seed": 1058625522,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "8B6G9Gz_7UoOlYaroTBKZ",
				"focus": 1.194139631697295,
				"gap": 8.300558976920854
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					7.105427357601002e-15,
					-133.8876953125
				]
			]
		},
		{
			"type": "arrow",
			"version": 1184,
			"versionNonce": 1117032852,
			"isDeleted": false,
			"id": "-LW-w7D4-JoRCN8xTf_Pz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 57.282339777127504,
			"y": -77.94476831522246,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 3.657551222655357,
			"height": 291.0083858644313,
			"seed": 701335986,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					-3.657551222655357,
					-291.0083858644313
				]
			]
		},
		{
			"type": "text",
			"version": 515,
			"versionNonce": 1273364908,
			"isDeleted": false,
			"id": "c4WID9CB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -201.36438873849755,
			"y": -15.565251713659961,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 18.539993286132812,
			"height": 25,
			"seed": 446006130,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "NNd79D1hMMXzbibR3sZv4",
					"type": "arrow"
				}
			],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A1",
			"rawText": "A1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A1",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 599,
			"versionNonce": 1320501012,
			"isDeleted": false,
			"id": "x58jlv8o",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 22.892012323514223,
			"y": 62.97960912618379,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 27.3599853515625,
			"height": 25,
			"seed": 733496626,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327157,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A2",
			"rawText": "A2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A2",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 509,
			"versionNonce": 294710316,
			"isDeleted": false,
			"id": "eZGnJAHs",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -210.4931118830288,
			"y": -401.2532094773319,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 57.03996276855469,
			"height": 25,
			"seed": 1338646706,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A1,C1)",
			"rawText": "(A1,C1)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A1,C1)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 581,
			"versionNonce": 1029979284,
			"isDeleted": false,
			"id": "qCfLXL7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 70.00347777761579,
			"y": -399.52869165506627,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 73.43995666503906,
			"height": 25,
			"seed": 585579122,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A3,C3)",
			"rawText": "(A3,C3)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A3,C3)",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 393,
			"versionNonce": 1736083116,
			"isDeleted": false,
			"id": "duPj73Whk7mypYDe-kCke",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -59.38426433182667,
			"y": -255.1586108852507,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffc9c9",
			"width": 15.598219043002732,
			"height": 15.598219043002732,
			"seed": 1320885298,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 356,
			"versionNonce": 1213696532,
			"isDeleted": false,
			"id": "a5fb3oRN",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -114.98559840365124,
			"y": -248.20357112602164,
			"strokeColor": "#e03131",
			"backgroundColor": "#ffc9c9",
			"width": 58.26914978027344,
			"height": 34.85149636384516,
			"seed": 556582386,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "MUiwUzKRK3BMSnFKTZN_g",
					"type": "arrow"
				},
				{
					"id": "AcRH8zpWh_5MOTMVVeYiG",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 27.881197091076125,
			"fontFamily": 1,
			"text": "(b,z)",
			"rawText": "(b,z)",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(b,z)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 276,
			"versionNonce": 843895084,
			"isDeleted": false,
			"id": "MIUZ6gI0",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -359.17092009939313,
			"y": -400.5999744949304,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 16.0159912109375,
			"height": 35,
			"seed": 1696952690,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "z",
			"rawText": "z",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "z",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 993,
			"versionNonce": 42280852,
			"isDeleted": false,
			"id": "qxhSyiTd",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 8.998488450709146,
			"y": -52.14750523673911,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 74.67994689941406,
			"height": 25,
			"seed": 974811954,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "yhrBMGoxTd2AbWbJFbbBf",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A2,C2)",
			"rawText": "(A2,C2)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A2,C2)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 913,
			"versionNonce": 829541292,
			"isDeleted": false,
			"id": "CiKKnW3B",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -44.690328019287676,
			"y": -95.6447518131265,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 15.098388671875,
			"height": 14.570647687037667,
			"seed": 881409266,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "8mcWa_3xAt9m5Z5bFShK7",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 11.656518149630134,
			"fontFamily": 1,
			"text": "A4",
			"rawText": "A4",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A4",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1334,
			"versionNonce": 807605524,
			"isDeleted": false,
			"id": "AcRH8zpWh_5MOTMVVeYiG",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -51.22903925737137,
			"y": -245.69275252839623,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 163.24897994400578,
			"seed": 301518514,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "a5fb3oRN",
				"focus": 1.1883469858990166,
				"gap": 5.487409366006432
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-163.24897994400578
				]
			]
		},
		{
			"type": "line",
			"version": 728,
			"versionNonce": 655586860,
			"isDeleted": false,
			"id": "0LGuFvKpk4gTxJ311V1FR",
			"fillStyle": "cross-hatch",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 457.88764411736656,
			"y": 2.887028747777265,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 214.16240153136937,
			"height": 180.04543815218182,
			"seed": 2013696366,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					214.16240153136937,
					95.18335017919654
				],
				[
					179.7927309161082,
					-84.86208797298528
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 1722,
			"versionNonce": 514282132,
			"isDeleted": false,
			"id": "qvbZCq2wZSe6zcadp1gSX",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 618.288877766375,
			"y": 3.689855289284253,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 1.554418858072836,
			"height": 158.07896656481438,
			"seed": 1749955502,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "QTE6NKBX",
				"focus": -2.334785589264853,
				"gap": 15.97528076171875
			},
			"endBinding": {
				"elementId": "lFaMtxAp",
				"focus": -1.2186546920155672,
				"gap": 6.64969659267274
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.554418858072836,
					-158.07896656481438
				]
			]
		},
		{
			"type": "arrow",
			"version": 2007,
			"versionNonce": 1822892,
			"isDeleted": false,
			"id": "poFZL7KuJy0iHfM2iFtT8",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 636.5463240554375,
			"y": -81.5114996911845,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 0.05417625971858797,
			"height": 184.8563232421875,
			"seed": 736595438,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "OtGw5FrE",
				"focus": 0.4160275263157891,
				"gap": 10.446928123189679
			},
			"endBinding": {
				"elementId": "NjR6Z4c9",
				"focus": 0.9280922907956247,
				"gap": 15.4404296875
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					-0.05417625971858797,
					-184.8563232421875
				]
			]
		},
		{
			"type": "text",
			"version": 781,
			"versionNonce": 2093473812,
			"isDeleted": false,
			"id": "NjR6Z4c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 633.9058745315117,
			"y": -306.808252620872,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 71.79994201660156,
			"height": 25,
			"seed": 1608228910,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "poFZL7KuJy0iHfM2iFtT8",
					"type": "arrow"
				},
				{
					"id": "vBH6uAGZZ7d2RKBc5KvSA",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A4,C4)",
			"rawText": "(A4,C4)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A4,C4)",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 740,
			"versionNonce": 2083838764,
			"isDeleted": false,
			"id": "o-IoWNRzu_WPiD7pr3Ngk",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 347.4408553054374,
			"y": 2.168309879128003,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 360.583984375,
			"seed": 56093870,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-360.583984375
				]
			]
		},
		{
			"type": "arrow",
			"version": 725,
			"versionNonce": 846904724,
			"isDeleted": false,
			"id": "dAFOao7OrgOj_5gpcETUq",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 347.4408705642265,
			"y": 0.6468865392842531,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 390.2522735595703,
			"height": 149.86297607421875,
			"seed": 257042158,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					390.2522735595703,
					149.86297607421875
				]
			]
		},
		{
			"type": "arrow",
			"version": 709,
			"versionNonce": 1439208876,
			"isDeleted": false,
			"id": "Ogqq-FJtyJJjQgZmPc_Vo",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 348.2015669753593,
			"y": 2.168309879128003,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 429.8100280761719,
			"height": 159.75238037109375,
			"seed": 210442542,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					429.8100280761719,
					-159.75238037109375
				]
			]
		},
		{
			"type": "text",
			"version": 673,
			"versionNonce": 955640596,
			"isDeleted": false,
			"id": "QTE6NKBX",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 634.2641585280937,
			"y": -15.960245095028995,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 23.088592529296875,
			"height": 56.82167020853201,
			"seed": 1382794094,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "qvbZCq2wZSe6zcadp1gSX",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 45.45733616682561,
			"fontFamily": 1,
			"text": "b",
			"rawText": "b",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "b",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1435,
			"versionNonce": 1778450476,
			"isDeleted": false,
			"id": "X0XZn6EAyQZawvH-YNfWb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 456.2543623855156,
			"y": 0.6469475744405031,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 2.842170943040401e-14,
			"height": 336.24078369140625,
			"seed": 1664951726,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "9jAPfy2H",
				"focus": -2.3950752117013723,
				"gap": 12.932342529296875
			},
			"endBinding": {
				"elementId": "nWTpXN9R",
				"focus": 0.9074703469359764,
				"gap": 10.982759504180649
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					2.842170943040401e-14,
					-336.24078369140625
				]
			]
		},
		{
			"type": "arrow",
			"version": 1812,
			"versionNonce": 1924098196,
			"isDeleted": false,
			"id": "Kspkbnf0ZsCKAsFUuBoQl",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 672.3004134109062,
			"y": 98.01981134397175,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 7.105427357601002e-15,
			"height": 133.8876953125,
			"seed": 271073262,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "gDJdSfc0",
				"focus": 1.194139631697295,
				"gap": 8.300558976920854
			},
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					7.105427357601002e-15,
					-133.8876953125
				]
			]
		},
		{
			"type": "arrow",
			"version": 1297,
			"versionNonce": 1623726764,
			"isDeleted": false,
			"id": "EYet3TjCh9GYJTCvJepoF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 727.8334334304375,
			"y": -53.36458807009075,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 3.657551222655357,
			"height": 291.0083858644313,
			"seed": 490082862,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": "dot",
			"endArrowhead": "dot",
			"points": [
				[
					0,
					0
				],
				[
					-3.657551222655357,
					-291.0083858644313
				]
			]
		},
		{
			"type": "text",
			"version": 647,
			"versionNonce": 56440340,
			"isDeleted": false,
			"id": "9jAPfy2H",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 469.1867049148125,
			"y": 9.014928531471753,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 18.539993286132812,
			"height": 25,
			"seed": 1699716206,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "X0XZn6EAyQZawvH-YNfWb",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A1",
			"rawText": "A1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A1",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 731,
			"versionNonce": 1422386476,
			"isDeleted": false,
			"id": "4hxp9smZ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 693.4431059768242,
			"y": 87.5597893713155,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 27.3599853515625,
			"height": 25,
			"seed": 2117983918,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A2",
			"rawText": "A2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A2",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 796,
			"versionNonce": 670389140,
			"isDeleted": false,
			"id": "oKLwdqIW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 754.7840559890312,
			"y": -53.63985662477825,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 26.739990234375,
			"height": 25,
			"seed": 1554634990,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A3",
			"rawText": "A3",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A3",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 697,
			"versionNonce": 2070568876,
			"isDeleted": false,
			"id": "bUHtca8n",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 740.5545714309258,
			"y": -353.3386817004333,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 73.43995666503906,
			"height": 25,
			"seed": 1862202734,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A3,C3)",
			"rawText": "(A3,C3)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A3,C3)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 392,
			"versionNonce": 246268180,
			"isDeleted": false,
			"id": "tC219JKW",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 311.38017355391685,
			"y": -354.40996454029755,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 16.0159912109375,
			"height": 35,
			"seed": 1840893550,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "z",
			"rawText": "z",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "z",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 1125,
			"versionNonce": 1289325100,
			"isDeleted": false,
			"id": "gDJdSfc0",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 679.5495821040191,
			"y": -27.567324991607393,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 74.67994689941406,
			"height": 25,
			"seed": 512872622,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "Kspkbnf0ZsCKAsFUuBoQl",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A2,C2)",
			"rawText": "(A2,C2)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A2,C2)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 1045,
			"versionNonce": 788299412,
			"isDeleted": false,
			"id": "OtGw5FrE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 625.8607656340223,
			"y": -71.06457156799482,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 15.098388671875,
			"height": 14.570647687037667,
			"seed": 1907329774,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "poFZL7KuJy0iHfM2iFtT8",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 11.656518149630134,
			"fontFamily": 1,
			"text": "A4",
			"rawText": "A4",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A4",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 1049,
			"versionNonce": 738157740,
			"isDeleted": false,
			"id": "B2xZSzeezmbhsythogAzP",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 70,
			"angle": 0,
			"x": 457.1128680868468,
			"y": -340.4164508613654,
			"strokeColor": "#1971c2",
			"backgroundColor": "#a5d8ff",
			"width": 215.2955604635034,
			"height": 296.88123074346214,
			"seed": 1193674030,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					215.2955604635034,
					296.88123074346214
				],
				[
					179.49924279967286,
					73.48050220217601
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "ellipse",
			"version": 662,
			"versionNonce": 563579924,
			"isDeleted": false,
			"id": "XXV1DrWZDPV6nI8j_2NBB",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 612.6453109027632,
			"y": -164.0966125256841,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffc9c9",
			"width": 15.598219043002732,
			"height": 15.598219043002732,
			"seed": 714741678,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 573,
			"versionNonce": 243658540,
			"isDeleted": false,
			"id": "lFaMtxAp",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 554.9244502515016,
			"y": -164.64851112764967,
			"strokeColor": "#e03131",
			"backgroundColor": "#ffc9c9",
			"width": 58.26914978027344,
			"height": 34.85149636384516,
			"seed": 51835374,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "qvbZCq2wZSe6zcadp1gSX",
					"type": "arrow"
				},
				{
					"id": "vBH6uAGZZ7d2RKBc5KvSA",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 27.881197091076125,
			"fontFamily": 1,
			"text": "(b,z)",
			"rawText": "(b,z)",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(b,z)",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1832,
			"versionNonce": 2020927892,
			"isDeleted": false,
			"id": "vBH6uAGZZ7d2RKBc5KvSA",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 620.2423356545163,
			"y": -156.5751387352484,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 163.24897994400578,
			"seed": 94108974,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "lFaMtxAp",
				"focus": 1.2419371365232281,
				"gap": 7.048735622741219
			},
			"endBinding": {
				"elementId": "NjR6Z4c9",
				"focus": 1.3806002760792238,
				"gap": 13.663538876995403
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-163.24897994400578
				]
			]
		},
		{
			"type": "text",
			"version": 671,
			"versionNonce": 618080684,
			"isDeleted": false,
			"id": "nWTpXN9R",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 453.615418402636,
			"y": -371.5765956211464,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 57.03996276855469,
			"height": 25,
			"seed": 344562162,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "X0XZn6EAyQZawvH-YNfWb",
					"type": "arrow"
				}
			],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(A1,C1)",
			"rawText": "(A1,C1)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(A1,C1)",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 159,
			"versionNonce": 382375700,
			"isDeleted": false,
			"id": "jFgNb7iTpXGq2QnO1wMW0",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -215.77706384107256,
			"y": -358.22093228363315,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 217.8905397720535,
			"height": 300.5771992508494,
			"seed": 303981618,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					217.8905397720535,
					300.5771992508494
				]
			]
		},
		{
			"type": "arrow",
			"version": 176,
			"versionNonce": 35460140,
			"isDeleted": false,
			"id": "s4y5meOMllgtmxZzC2MTh",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -26.725492082966866,
			"y": -59.329021007414724,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 48.00287629654542,
			"height": 48.00284280608878,
			"seed": 104971890,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-7.930875050257043,
					-8.348300102520056
				],
				[
					-11.270241977904448,
					-21.70563385128287
				],
				[
					-7.9309085407137445,
					-36.31517577592132
				],
				[
					6.678666874381406,
					-46.33317608749343
				],
				[
					17.114058747759827,
					-48.00284280608878
				],
				[
					28.384300725664218,
					-46.75060113975644
				],
				[
					36.732634318640976,
					-40.071934265375035
				]
			]
		},
		{
			"type": "arrow",
			"version": 218,
			"versionNonce": 151950484,
			"isDeleted": false,
			"id": "rt78TZcxYgyPy5wDYod9v",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 51.748776710101254,
			"y": -364.8775448987416,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 83.06581040613443,
			"height": 75.55231017245535,
			"seed": 368163374,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327158,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-83.06581040613443,
					75.55231017245535
				]
			]
		},
		{
			"type": "text",
			"version": 416,
			"versionNonce": 1145155244,
			"isDeleted": false,
			"id": "5vI4zCbj",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 66.29385195598414,
			"y": -348.370397697888,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 93.97857666015625,
			"height": 55.44146764114398,
			"seed": 929476974,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 22.176587056457592,
			"fontFamily": 1,
			"text": "3 exits\n4 enters",
			"rawText": "3 exits\n4 enters",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3 exits\n4 enters",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 493,
			"versionNonce": 413276716,
			"isDeleted": false,
			"id": "FlLOO3_O7QVmApz-llBD-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 144.0983160000989,
			"y": -170.3655870878048,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 157.17108424672682,
			"height": 0.8393621472244774,
			"seed": 61064110,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714156484819,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					157.17108424672682,
					0.8393621472244774
				]
			]
		},
		{
			"type": "text",
			"version": 305,
			"versionNonce": 340764180,
			"isDeleted": false,
			"id": "OiIGhcTp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 112.69123574797399,
			"y": -138.21459642788273,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 219.98524475097656,
			"height": 90.21450195312502,
			"seed": 1173194674,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714156461275,
			"link": null,
			"locked": false,
			"fontSize": 24.05720052083334,
			"fontFamily": 1,
			"text": "By pivoting to\na lower costing\ncolum we improve z",
			"rawText": "By pivoting to\na lower costing\ncolum we improve z",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "By pivoting to\na lower costing\ncolum we improve z",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 159,
			"versionNonce": 2038005652,
			"isDeleted": false,
			"id": "py1Iwv29h7h9LuCc-upsy",
			"fillStyle": "solid",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -202.7567370033538,
			"y": -298.6035257849034,
			"strokeColor": "#f08c00",
			"backgroundColor": "transparent",
			"width": 51.9023309403932,
			"height": 37.073084219201405,
			"seed": 1475033646,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-3.4601582923620526,
					-4.448771844105693
				],
				[
					-4.9430894812368535,
					-14.829224998673055
				],
				[
					-1.977237964746621,
					-23.23247277227199
				],
				[
					3.460147431102655,
					-30.1527676344773
				],
				[
					10.87477079169858,
					-36.084470667457765
				],
				[
					20.76092803165352,
					-37.073084219201405
				],
				[
					28.669858168121237,
					-35.09585711571418
				],
				[
					39.050322183948026,
					-29.164154082733717
				],
				[
					46.959241459156345,
					-18.783700928166297
				]
			]
		},
		{
			"type": "text",
			"version": 594,
			"versionNonce": 344912812,
			"isDeleted": false,
			"id": "YHodX06X",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -291.366808197764,
			"y": -252.719491158148,
			"strokeColor": "#E1E1E1",
			"backgroundColor": "transparent",
			"width": 165.3065185546875,
			"height": 143.91664575011296,
			"seed": 1528961586,
			"groupIds": [
				"435CEoCs"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 23.026663320018073,
			"fontFamily": 1,
			"text": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"rawText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 594,
			"versionNonce": 1993108756,
			"isDeleted": false,
			"id": "R6EDW1Ok",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -289.44791958776244,
			"y": -252.719491158148,
			"strokeColor": "#E1E1E1",
			"backgroundColor": "transparent",
			"width": 165.3065185546875,
			"height": 143.91664575011296,
			"seed": 1528961586,
			"groupIds": [
				"435CEoCs"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 23.026663320018073,
			"fontFamily": 1,
			"text": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"rawText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 594,
			"versionNonce": 2085330476,
			"isDeleted": false,
			"id": "uIqHG9k6",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -290.4073638927632,
			"y": -251.76004685314726,
			"strokeColor": "#E1E1E1",
			"backgroundColor": "transparent",
			"width": 165.3065185546875,
			"height": 143.91664575011296,
			"seed": 1528961586,
			"groupIds": [
				"435CEoCs"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 23.026663320018073,
			"fontFamily": 1,
			"text": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"rawText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 594,
			"versionNonce": 1986374292,
			"isDeleted": false,
			"id": "BuQg68lL",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -290.4073638927632,
			"y": -253.67893546314875,
			"strokeColor": "#E1E1E1",
			"backgroundColor": "transparent",
			"width": 165.3065185546875,
			"height": 143.91664575011296,
			"seed": 1528961586,
			"groupIds": [
				"435CEoCs"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 23.026663320018073,
			"fontFamily": 1,
			"text": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"rawText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 594,
			"versionNonce": 1276049580,
			"isDeleted": false,
			"id": "S583Ulr3",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -290.4073638927632,
			"y": -252.719491158148,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 165.3065185546875,
			"height": 143.91664575011296,
			"seed": 1528961586,
			"groupIds": [
				"435CEoCs"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1714151327159,
			"link": null,
			"locked": false,
			"fontSize": 23.026663320018073,
			"fontFamily": 1,
			"text": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"rawText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "the simplex\npivots around\nthe m-1 simplex\nthat acts as\nan anchor",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 4,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 406.0677876539132,
		"scrollY": 455.8853084274925,
		"zoom": {
			"value": 1.35
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		},
		"objectsSnapModeEnabled": false
	},
	"files": {}
}
```
%%
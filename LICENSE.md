This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

Copyright (c) Luca Solbiati, 2024

Permissions beyond the scope of this license may be obtained by contacting the author via email at lucasolbiati98@gmail.com
